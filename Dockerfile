ARG IMAGE_PREFIX=""

FROM ${IMAGE_PREFIX}python:3.12-slim

RUN set -ex; \
    DEBIAN_FRONTEND=noninteractive apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        patchelf \
        postgresql-common \
        gcc \
        libc6-dev \
        libpq-dev; \
    pip install --no-cache-dir --upgrade pip; \
    pip install --no-cache-dir auditwheel build twine;
